require "rinruby"

module Morris

	# Compute the sensitivities for the given model via the Morris method
	# model is a method (obtained by a call to method(:mymodelfunction)) representing the model to test
	# model should take, as input, a vector of length n_params
	# n_repetitions is the number of repetitions that will be performed during the Morris sampling
	# param_lower_bounds and param_upper_bounds are both vectors of length n_params, giving the upper
	# and lower bounds for each parameter
	#
	#
	# example usage:
	#
	# def myfun(x):
	#	return x[0]+10*x[1]
	# end
	# Morris.compute_sensitivities(methd(:myfun), 2, 10, [0,0], [1,1])
	# => [1.0, 10.0]
	#
	def Morris.compute_sensitivities(model, n_params, n_repetitions, param_lower_bounds, param_upper_bounds)

		R.assign("param_lower_bounds", param_lower_bounds)
		R.assign("param_upper_bounds", param_upper_bounds)

		R.eval <<EOF
library("sensitivity")
result <- morris(NULL, #{n_params}, #{n_repetitions}, binf=param_lower_bounds, bsup=param_upper_bounds, scale=TRUE, design = list(type = "oat", levels = 11, grid.jump = 1))
EOF

		resultX = R.pull("as.numeric(result$X)")

		results = []
		stride = resultX.length/n_params
		for i in 0..(stride-1)
			x = []
			for j in 0..(n_params-1)
				x << resultX[i+j*stride]
			end
			results << model.run(x)
		end

		R.assign("yres", results)

		R.eval <<EOF
tell(result, yres)
mu <- apply(result$ee, 2, mean)
EOF

		return R.mu
	end
end
