# C:\bayesian\tests\TestFigures3to6.R
# Jusko - 11-14-2014
# $Author: jusko $
# $Date: 2014-12-05 16:56:34 -0600 (Fri, 05 Dec 2014) $
# $Revision: 14 $

theta_info <- list()
theta_info[[1]] <- list()
theta_info[[1]]$name <- '1 Plugload - Occupied (W/m^2)'
theta_info[[1]]$abbr = "\n\n\nPlug\nLd\nOcc";
theta_info[[1]]$prior <- list();
theta_info[[1]]$prior$type <- 'triangular';
theta_info[[1]]$prior$params <- c(6.0, 34.0, 17.5);

theta_info[[2]] <- list()
theta_info[[2]]$name <- '2 Infiltration Rate (m^3/h/m^2 @75Pa)'
theta_info[[2]]$abbr = "\n\n\nInfil\nRate";
theta_info[[2]]$prior <- list();
theta_info[[2]]$prior$type <- 'triangular';
theta_info[[2]]$prior$params <- c(1, 15, 6);

theta_info[[3]] <- list()
theta_info[[3]]$name <- '3 Lighting Power Density (W/m^2)'
theta_info[[3]]$abbr = "\n\n\nLight\nPow\nDens";
theta_info[[3]]$prior <- list();
theta_info[[3]]$prior$type <- 'triangular';
theta_info[[3]]$prior$params <- c(11.9, 17, 14.45);

theta_info[[4]] <- list();
theta_info[[4]]$name <- '4 Heating Temperature Setpoint ( \u030aC)'; # (\\circC)
theta_info[[4]]$abbr = "\n\n\nHeat\nTemp\nSet Pt";
theta_info[[4]]$prior <- list();
theta_info[[4]]$prior$type <- 'triangular';
theta_info[[4]]$prior$params <- c(20.2, 24.2, 22.2);

num_theta = length (theta_info);

for(iter in 1:num_theta)
{
  theta_info[[iter]]$min <- theta_info[[iter]]$prior$params[1]
  theta_info[[iter]]$max <- theta_info[[iter]]$prior$params[2]
  #theta_info[[iter]]$start <- (theta_info[[iter]]$min + theta_info[[iter]]$max) / 2.0
}

# pvals = read.csv(file.path("d:/anl/bayesian", "pvals.csv"));
# pvals = read.csv(file.path("c:/bayesian/YeonsookRConversion", "pvals.csv"));
 pvals = read.csv("pvals.csv");
#pvals = as.matrix(pvls))
#nmcmc = 30000; # ???? why is this needed? mjj
burnin = 1000;
pvals = tail(pvals, length(pvals[1])-burnin); # significant difference when showing burnin-data

#------------------------
library (ggplot2);
library (triangle);
library(gridExtra); # http://www.r-bloggers.com/extra-extra-get-your-gridextra/
library(car);
#------------------------
nBins = 32; # good: 100
dev.off(2);
#dev.new();
# windows();

plots  = list(); # List of plots, later to be up in grid
plots2 = list(); # List of plots, later to be up in grid
Vs		 = list(); # List of V1,V2, ... Vn
# i = 4;
# for (i in 1:4)
for (i in 1:num_theta)
{
 windows();
 max_x = theta_info [[i]]$max;
 min_x = theta_info [[i]]$min;
 rang_x =  max_x - min_x;
 pvals[[i]] = pvals[[i]] * rang_x + min_x; # normalize pvals between min_x and max_x
 histo = hist(pvals[[i]], breaks=seq(min_x, max_x, l=nBins), plot=FALSE); # good: 50 bins

                                       # histo = histogram of pvals[i]
                                       # get total area of histogram
 pval_area = 0;
 delta_x = histo$mids[2] - histo$mids[1]; # width of histogram bars
 for (c in 1:length (histo$counts))
    {
     pval_area = pval_area + histo$counts[c] * delta_x;
    }

                                       # create triangular distribution

x = seq (min_x, max_x, length.out=100); # good: 100 for straighter lines
y = dtriangle (x, theta_info[[i]]$prior$params[1], theta_info[[i]]$prior$params[2],
                  theta_info[[i]]$prior$params[3]);

													# Area of triangle is 1, scale it's height so that
													# the area becames the area of the original histogram.

ty = y * pval_area; # = altitude of ttiangle
prior = data.frame (x, ty); # the prior (triangular) distribution

Vs = c(Vs, pvals[[i]]); #---

                                       # plot the histogram and triangular dist
 plot1 = ggplot() +
      geom_histogram (data=pvals, aes(x=pvals[[i]]), binwidth=delta_x) +
      geom_line (data=prior, aes(x=x, y=ty),  color="red", size=1) +
		xlim (min(pvals[i], min_x),max(pvals[i], max_x)) +
      labs (x=theta_info[[i]]$name);
if (FALSE) # Shows very strange behaviour!!!!
{
 plots  = c(plots, list(plot1));
}
 print (plot1);
 message ("--- plot ", i, " ---");
 message ("pval_area = ", pval_area);
 message ("nBins = ", nBins);
 message ("x range: ", min_x, " to ", max_x);
}
if (FALSE) # Shows very strange behaviour!!!!
	{
	windows();
	do.call(grid.arrange, c(plots, ncol=4, main=paste("bandwidth =", delta_x)));
	}


#------------------------------

if (TRUE)  # include for Matt R. delivery
{
windows();
scatterplotMatrix(~V1+V2+V3+V4, data=pvals, smoother=FALSE, diagonal="histogram", pch='.',
						col="black", reg.line=FALSE, var.labels=NULL); # main="title"
windows();
scatterplotMatrix(~V1+V2+V3+V4, data=pvals, smoother=FALSE, diagonal="density", pch='.',
						reg.line=FALSE,
						var.labels=c(theta_info[[1]]$abbr, theta_info[[2]]$abbr, theta_info[[3]]$abbr, theta_info[[4]]$abbr));
}
