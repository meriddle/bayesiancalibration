require 'rinruby'

module BCRunner
	def BCRunner.runBC(params, com_filename, field_filename, numWeatherVars, numMCMCSteps, pvals_filename)

		R.eval("theta_info <- list()")
		for i in 1..params.length
			R.assign("i", i)
			R.assign("name", params[i-1].name)
			R.assign("prior_type", params[i-1].dist)
			prior_params = []
			prior_params << params[i-1].min
			prior_params << params[i-1].max
			prior_params << params[i-1].mid
			R.assign("prior_params", prior_params)
			R.eval("theta_info[[i]] <- list()")
			R.eval("theta_info[[i]]$name <- name")
			R.eval("theta_info[[i]]$prior <- list()")
			R.eval("theta_info[[i]]$prior$type <- prior_type")
			R.eval("theta_info[[i]]$prior$params <- prior_params")
		end

		R.eval("source('runmcmc.R')")
		R.assign("com_filename", com_filename)
		R.assign("field_filename", field_filename)
		R.assign("numWeatherVars", numWeatherVars)
		R.assign("numMCMCSteps", numMCMCSteps)
		R.assign("pvals_filename", pvals_filename)
		R.eval("runmcmc(theta_info, com_filename, field_filename, numWeatherVars, numMCMCSteps, pvals_filename)")
		
	end
end