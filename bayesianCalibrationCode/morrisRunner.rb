require 'openstudio'
require 'morris'
require 'modelRunner'

module MorrisRunner
	def MorrisRunner.runMorris(morrisModel, parameters, numRunsPerParameter)
		# Run the Morris method
		sensitivities = Morris.compute_sensitivities(morrisModel, parameters.length, numRunsPerParameter, parameters.map {|x| x.min}, parameters.map {|x| x.max})

		# Display sensitivities
		puts
		puts "*****************************************"
		puts "Morris method sensitivities (normalized):"
		puts "*****************************************"
		puts
		parameters.zip(sensitivities) do |p,s|
			printf("%40s: %8.4f\n", p.name, s)
		end
		return sensitivities
	end
	def MorrisRunner.selectParameters(parameters, sensitivities, numSelectedParameters)
		parameterMap = []
		(0..parameters.length-1).each() do |i|
			parameterMap << i
		end
		combined = []
		(0..parameters.length-1).each() do |i|
			combined << [parameters[i], sensitivities[i], parameterMap[i]]
		end
		
		combined.sort! {|x,y| y[1].abs <=> x[1].abs}
		selectedParameters = []
		numSelectedParameters = [numSelectedParameters, parameters.length].min
		puts
		puts "*****************************************"
		puts "Selected parameters:"
		puts "*****************************************"
		puts
		sortedParameterMap = []
		(0..numSelectedParameters-1).each() do |i|
			sortedParameterMap << combined[i][2]
			selectedParameters << combined[i][0]
			printf("%40s: %8.4f\n", selectedParameters[i].name, combined[i][1])
		end
		return [selectedParameters, sortedParameterMap]
	end
end