#This executes the parameter screening and experiment design steps
#Inputs are the ism model, the parameter input file, and the output 

require 'parameterInput'
require 'rinruby'

# load parameters
params = ParameterInput.read_input_file(ARGV[0])
R.eval("params=list()")
for i in 1..params.length
	R.assign("i", i)
	R.assign("name", params[i-1].name)
	R.eval("params[[i]]=list()")
	R.eval("params[[i]]$name = name")
end

for i in 1..params.length
	R.assign("i", i)
	R.eval("name2 = params[[i]]$name")
	name2 = R.pull("name2")
	puts name2
end
