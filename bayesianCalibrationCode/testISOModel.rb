require 'openstudio'
require 'parameterInput'
require 'morrisRunner'
require 'lhsRunner'

# Initialize model and load parameters
userModel = OpenStudio::ISOModel::UserModel.new
userModel.load(OpenStudio::Path.new(ARGV[0]))
simisoModel= userModel.toSimModel
simResults = simisoModel.simulate

monthly_elec_use=[]
(0..11).each do |month|
	elec_use = 0
	(0..13).each do |cat|
		elec_use = elec_use + simResults.monthlyResults[month].getEndUse(OpenStudio::EndUses.fuelTypes[0], OpenStudio::EndUses.categories[cat])
	end
	monthly_elec_use << elec_use
	printf("%8.4f", elec_use);
end
#		(0..numSelectedParameters-1).each() do |i|
#			selectedParameters << combined[i][0]
#			printf("%40s: %8.4f\n", selectedParameters[i].name, combined[i][1])
#		end

parameters = ParameterInput.read_input_file(ARGV[1])
puts("")

# Run morris method to calculate sensitivities for all parameters
numRunsPerParameter = 10
userModel2 = userModel
sensitivities = MorrisRunner.runMorris(userModel2, parameters, numRunsPerParameter)

#userModel = OpenStudio::ISOModel::UserModel.new
userModel.load(OpenStudio::Path.new(ARGV[0]))
simisoModel= userModel.toSimModel
simResults = simisoModel.simulate

monthly_elec_use=[]
(0..11).each do |month|
	elec_use = 0
	(0..13).each do |cat|
		elec_use = elec_use + simResults.monthlyResults[month].getEndUse(OpenStudio::EndUses.fuelTypes[0], OpenStudio::EndUses.categories[cat])
	end
	monthly_elec_use << elec_use
	printf("%8.4f", elec_use);
end

