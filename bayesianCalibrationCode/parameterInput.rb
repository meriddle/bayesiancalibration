require "CSV"
require "rinruby"

module ParameterInput

	class Parameter
		attr_reader :name
		attr_reader :dist
		attr_reader :min
		attr_reader :max
		attr_reader :mid
		attr_reader :std
		
		def initialize(name, dist, min, max, mid, std)
			@name = name
			@dist = dist
			@min = min.to_f
			@max = max.to_f
			@mid = mid.to_f
			@std = std.to_f
		end
	end

	def ParameterInput.read_input_file(filename)
		parameters = []
		CSV.foreach(filename) do |name,dist,min,max,mid,std|
			if name != nil && name.match("^#") == nil
				parameters << Parameter.new(name,dist,min,max,mid,std)
			end
		end
		return parameters
	end
	def ParameterInput.getMidValues(parameters)
		midValues = []
		for i in 0..parameters.length-1
			midValues << parameters[i].mid
		end
		return midValues
	end

end
