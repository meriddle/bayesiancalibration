
require 'parameterInput'
require 'morrisRunner'
require 'lhsRunner'

# load parameters
parameters = ParameterInput.read_input_file(ARGV[1])

# Run morris method to calculate sensitivities for all parameters
numRunsPerParameter = Integer(ARGV[0])
morrisSample = MorrisRunner.runMorris(parameters, numRunsPerParameter)

# save parameter values to file
LhsRunner.writeToFile2(parameters, morrisSample, ARGV[2])

