
#===============================================================%
#     Kennedy and O'Hagan Model for Combining Field Data and    %
#       Computer Simulations for Calibration and Prediction     %    
#===============================================================%
  
# GASPCOV Generate GASP covariance matrix
#
#         Use this function to:
#           1. Evaluate GASP covariance matrix

# CALLED BY: gaspchcov.m, etaxtpred.m, etaxpred.m, yxpred.m

#==============================================================%
#                        REQUIRED INPUTS                       %
#==============================================================%
# dist structure containing 
#   d: ({n choose 2} x p) distance matrix (x(k)-x'(k))^alpha
#      for an n x p design matrix x
#   n: original dimension of design matrix
#   odut: indices of off-diagonal-upper-triangle elements of
#         an nxn matrix

# beta: parameters for strength of dependencies
                                           
# lam: precision parameter
                                           
#===============================================================%
#                           OUTPUTS                             %
#===============================================================%
# sigma: GASP covariance matrix  (n x n)
                                           
#===============================================================%
#                          MODEL DETAILS                        %
#===============================================================%
                                           
# sigma = 1/lam C(x,x')
# C(x,x') = exp{-sum_{k=1:p}beta(k)(x(k)-x'(k))^alpha}

# COMMENTS: Generally assume alpha = 2

#===============================================================#

gaspcovFromTri <- function(dist, odutUT, odutLT, beta, lam, tri){
    
  n = dist$n;
  d = dist$d;
  #odut = dist$odut;
  
  # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  # Specify size of covariance matrix corresponding to design matrix
  sigma <- matrix (0, nrow = n, ncol=n);    #indi=zeros(inds,1);
  
  #temp1 <- exp(-d%*%beta)/lam
  # Set upper triangle of C(x,x')
  sigma[odutUT] = tri;
  
  # Set lower triangle of C(x,x')
  #sigma <- sigma + t(sigma);
  sigma[odutLT] = tri;
  
  # Set diagonal elements of C(x,x')
  diags = 1:n
  diags <- diags * (n+1) - n
  #diags = [0:n:n*(n-1)] + [1:n];
  sigma[diags] = 1/lam;
  
  return(sigma)
}
