
#===============================================================%
#     Kennedy and O'Hagan Model for Combining Field Data and    %
#       Computer Simulations for Calibration and Prediction     %    
#===============================================================%
  
# GENDIST Function to create a distance structure

#         Use this function to:
#            1. Store information on size of original design matrix
#            2. Create and store distance array
#            3. Save indices of off-diagonal upper triangle 
#               elements of an n x n matrix

# CALLED BY: gaspdriver.m, etaxtpred.m, etaxpred.m,
#            yxpred.m, gaspmcmc.m

#==============================================================%
#                        REQUIRED INPUTS                       %
#==============================================================%
# x: design matrix (n x p)

#===============================================================%
#                           OUTPUTS                             %
#===============================================================%
# dist data structure containing
#     n: number of rows in design matrix
#     d: {n choose 2}xp distance matrix obtained as
#        (xik - xjk)^alpha for all pairs of rows (i,j) of x

# COMMENTS: Generally assume alpha = 2

#     odut: indices corresponding to off-diagonal upper
#           triangle entries of an nxn matrix

#===============================================================%

#   generates the nxnxp distance array values and supporting
#   information, given the nxp location matrix x
#   or if a d is passed in, just update the distances

gendist <- function (x, inds)
{
  dist = list ();
  dist$n = dim(x) [1];
  #p = dim(x) [2];
  #dist$n = n
  # Create distance array with alpha = 2
  #alpha = 2;
  dist$d = (x[inds$j,] - x[inds$i,]) ^ 2;  # d.d=(data(indj,:)-data(indi,:)).^alpha;
  #browser (); ###############################
  #dist$odut =inds$i + dist$n*(inds$j-1);  
 
  return (dist);
}
