
require 'parameterInput'
require 'lhsRunner'

# load parameters
parameters = ParameterInput.read_input_file(ARGV[1])

# genereate LHS sample
numModelRuns = Integer(ARGV[0])
lhsSample = LhsRunner.generateLHSSample(parameters, numModelRuns)

# save parameter values to file
LhsRunner.writeToFile2(parameters, lhsSample, ARGV[2])

