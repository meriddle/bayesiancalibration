graphPred <- function(com_filename, field_filename, expred_filename, yyxpred_filename){
	#message("in graphPred")
	#need to pass it filenames, and do a simpler calculation of ym, ysd, yf based on field and com files
	#also need to change plot labels for scatterplot (so not abbr)
	expred = as.matrix(read.csv(expred_filename));
	yyxpred = as.matrix(read.csv(yyxpred_filename));

	DATACOMP <- read.csv(com_filename, header=FALSE, sep="\t")
	DATACOMP <- as.matrix(DATACOMP)
	  
	DATAFIELD <- read.csv(field_filename, header=FALSE, sep="\t")
	DATAFIELD <- as.matrix(DATAFIELD)
    
	yInds <- 1
	yf <- DATAFIELD[,yInds,drop=FALSE]
	yc <- DATACOMP[,yInds,drop=FALSE]
	ym <- apply(yc, 2, mean)  
	ysd <- apply(yc, 2, sd) 
	
	# Average over realizations
	require("plyr")
	# mmexpred = mean(expred);
	# mmexpred = ((yyxpred[[1]])[1,] + (yyxpred[[1]])[2,]+ (yyxpred[[1]])[3,])  / 3; # mmyxpred = mean(yyxpred);
	mmexpred = aaply(expred, 2, mean);
	mmyxpred = aaply(yyxpred, 2, mean);
	sdev     = aaply(yyxpred*ysd+ym, 2, sd); # 1 x 12
	sdev <<- sdev
	#message("just defined sdev")
	# varpred = var(yyxpred);  # THERE WAS A MISTAKE! MEAN WAS USED(OLD ONE: var(myyxpred)
	varpred = aaply(yyxpred, 2, var);
	sepred = sqrt(varpred);
	means = mmyxpred * ysd + ym;

	#################
	require("ggplot2");
	lv=month.abb[1:12];
	Month=factor(lv, levels=lv);

	#yf=t(yf);
	#browser();
	df = data.frame (Month, Means=means, history=yf[,1]);

	plot12 = ggplot() +
	  geom_errorbar(data=df, mapping=aes(x=Month, ymin=Means-2*sdev, ymax=Means+2*sdev),
					width=0.2, size=1, color="blue") +
	  geom_point(data=df, mapping=aes(x=Month, y=Means), size=4, shape=21, fill="black") +
	  geom_point(data=df, mapping=aes(x=Month, y=history), size=4, shape=21, fill="white") +
	  ggtitle ("testPred 1");
	#message("just defined plot using sdev")
	windows ();
	
    jpeg("predRanges.jpg")
	#message("before print plot")
	print (plot12);
	#message("after print plot")
	dev.off()
	#################

	#
	# figure(12)
	# % plot(1:12,mmyxpred*ysd+ym, 'ro', 'MarkerSize',7);
	# errorbar(1:12,mmyxpred*ysd+ym, 2*std(yyxpred*ysd+ym), 2*std(yyxpred*ysd+ym),'ro','MarkerSize',3,'MarkerFaceColor','r');
	# hold on;
	# xlabel('Month'); ylabel('Total Energy Consumption'); hold on;
	# % Plot computer responses and field responses
	# plot(1:12,yf,'ro','MarkerSize',5); hold on;
	#message ("Done with graphPred.R");

	#Rprof(NULL)
	#summaryRprof("Run2.rprof")
}