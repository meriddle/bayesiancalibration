# C:\bayesian\tests\TestFigures3to6.R
# Jusko - 11-14-2014
# $Author: jusko $
# $Date: 2014-12-05 16:56:34 -0600 (Fri, 05 Dec 2014) $
# $Revision: 14 $

source("graphPosteriors.r")

theta_info <- list()
theta_info[[1]] <- list()
theta_info[[1]]$name <- '1 Plugload - Occupied (W/m^2)'
theta_info[[1]]$abbr = "\n\n\nPlug\nLd\nOcc";
theta_info[[1]]$prior <- list();
theta_info[[1]]$prior$type <- 'triangular';
theta_info[[1]]$prior$params <- c(6.0, 34.0, 17.5);

theta_info[[2]] <- list()
theta_info[[2]]$name <- '2 Infiltration Rate (m^3/h/m^2 @75Pa)'
theta_info[[2]]$abbr = "\n\n\nInfil\nRate";
theta_info[[2]]$prior <- list();
theta_info[[2]]$prior$type <- 'triangular';
theta_info[[2]]$prior$params <- c(1, 15, 6);

theta_info[[3]] <- list()
theta_info[[3]]$name <- '3 Lighting Power Density (W/m^2)'
theta_info[[3]]$abbr = "\n\n\nLight\nPow\nDens";
theta_info[[3]]$prior <- list();
theta_info[[3]]$prior$type <- 'triangular';
theta_info[[3]]$prior$params <- c(11.9, 17, 14.45);

theta_info[[4]] <- list();
theta_info[[4]]$name <- '4 Heating Temperature Setpoint ( \u030aC)'; # (\\circC)
theta_info[[4]]$abbr = "\n\n\nHeat\nTemp\nSet Pt";
theta_info[[4]]$prior <- list();
theta_info[[4]]$prior$type <- 'triangular';
theta_info[[4]]$prior$params <- c(20.2, 24.2, 22.2);

graphPosteriors(theta_info, 'pvals.csv', 1000)

