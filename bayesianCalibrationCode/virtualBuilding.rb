module VirtualBuilding
	def VirtualBuilding.generateRandomVals(parameters)
		#prng = Random.new
		vals = []
		for i in 0..parameters.length-1
			min = parameters[i].min
			max = parameters[i].max
			val = min + rand * (max-min)
			#val = min + prng.rand * (max-min)
			vals << val
		end
		return vals
	end
	def VirtualBuilding.writeArrayToFile(results, filename)
		File.open(filename, "w+") do |f|
			#for resultsRow in results
				for r in results
					f.write(r)
					f.write("\t")
				end
				f.write("\n")
			#end
		end	
		puts		
		puts "run results have been written to #{filename}"
	end
	def VirtualBuilding.addRandomVariation(monthlyEnergyData, maxPercentChange)
		vals = []
		for i in 0..monthlyEnergyData.length-1
			min = monthlyEnergyData[i] * (1-maxPercentChange)
			max = monthlyEnergyData[i] * (1+maxPercentChange)
			val = min + rand * (max-min)
			vals << val
		end		
		return vals
	end
	def VirtualBuilding.generateFieldFile(observedData, weatherData, filename)
		results = []
		for m in 0..(observedData.length - 1)
			#puts "energy use month #{m} = #{monthlyEnergy[m]}"			
			resultsRow = []
			resultsRow << observedData[m]
			#add weather variables
			for j in 0..(weatherData[m].length-1)
				resultsRow << weatherData[m][j]
			end
			results << resultsRow
		end
		LhsRunner.writeToFile(results, filename)		
	end
end
#trueParameterVals = VirtualBuilding.generateRandomVals(selectedParameters)
#VirtualBuilding.writeToFile(trueParameterVals, ARGV[4])
#monthlyEnergyData << energyModel.runMonthly(trueParameterVals)
#observedData = VirtualBuilding.addRandomVariation(monthlyEnergyData)
#VirtualBuilding.generateFieldFile(observedData, weather, ARGV[3])
