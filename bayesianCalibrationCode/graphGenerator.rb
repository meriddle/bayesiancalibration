require 'rinruby'

module GraphGenerator
	def GraphGenerator.graphPosteriors(params, pvals_filename, burnin)

		R.eval("theta_info <- list()")
		for i in 1..params.length
			R.assign("i", i)
			R.assign("name", params[i-1].name)
			R.assign("prior_type", params[i-1].dist)
			prior_params = []
			prior_params << params[i-1].min
			prior_params << params[i-1].max
			prior_params << params[i-1].mid
			R.assign("prior_params", prior_params)
			R.eval("theta_info[[i]] <- list()")
			R.eval("theta_info[[i]]$name <- name")
			R.eval("theta_info[[i]]$prior <- list()")
			R.eval("theta_info[[i]]$prior$type <- prior_type")
			R.eval("theta_info[[i]]$prior$params <- prior_params")
		end

		R.eval("source('graphPosteriors.R')")
		#R.assign("com_filename", com_filename)
		#R.assign("field_filename", field_filename)
		R.assign("pvals_filename", pvals_filename)
		R.assign("burnin", burnin)
		R.eval("graphPosteriors(theta_info, pvals_filename, burnin)")
		
	end
	def GraphGenerator.graphPred(com_filename, field_filename, expred_filename, yyxpred_filename)

		R.eval("source('graphPred.R')")
		#R.assign("com_filename", com_filename)
		#R.assign("field_filename", field_filename)		
		#R.assign("expred_filename", expred_filename)
		#R.assign("yyxpred_filename", yyxpred_filename)
		#R.eval("graphPred(com_filename, field_filename, expred_filename, yyxpred_filename)")
		R.eval("graphPred('cal_example_com.txt', 'cal_example_field.txt', 'expred.csv', 'yyxpred.csv')")
		
	end
end