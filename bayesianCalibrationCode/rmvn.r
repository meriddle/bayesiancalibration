# C:\bayesian\YeonsookRConversion2\rmvn.r
# Converted from M code.
# Original : M.Jusko - Dec 8, 2014
# Last Mod: M. Jusko - Dec. 8, 2014 - 12:45pm

rmvn = function (mu,covmat)
{

# RMVN: Generate a realization from a multivariate normal distribution
#       with mean mu and covariance matrix covmat.

# ==============================================================%
#                         REQUIRED INPUTS                       %
# ==============================================================%
#  mu: mean vector column --> (p x 1)
# covmat: covariance matrix --> (p x p)
# ===============================================================%
#                            OUTPUTS                             %
# ===============================================================%
#  rnorm: realization from multivariate normal distribution
#         with mean mu and covariance matrix covmat --> (p x 1)
# ==============================================================%

# Length of mean vector
#message ("      ... Inside rmvn()...");
p = length(mu);

# Get square root of covariance matrix using singular value
# decomposition -- more numerically stable

tmp = svd(covmat); # [U S V] = svd(covmat);
U=tmp$u;
V=tmp$v;
S=diag(tmp$d)

chcov = U%*%(sqrt(S)); # matlab: matrix mul, not cell x cell

# Generate random values from multivariate normal with mean
#% 0 and covariance matrix I
z=rnorm(p); # matlab:  z = randn([p 1]);

# Use mean vector mu and square root of covariance matrix to
# transform to desired multivariate normal distribution
rn = mu + chcov %*% z; # matlab:  rnorm = mu + chcov*z;
return (rn);
}
