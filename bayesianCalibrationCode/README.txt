This is a basic framework for conducting parmater screening via the Morris method and experiment design via LHS sampling in Ruby/R, using the ISO model provided by OpenStudio.  

An example of running the tool is as follows:

$ ruby runAll.rb examplemodel.ism test_inputs.csv cal_example_com.txt cal_example_field.txt true_params.txt pvals.csv

Here, examplemodel.ism is the base ISO model to be simulated, 
test_inputs.csv is a CSV file specifying the names and ranges of the parameters whose sensitivities are to be estimated.
cal_example_com.txt is the name of a file that the computer simulation results will be written to.  
cal_example_field.txt is the name of a file that simulated field data will be written to. 
true_params.txt is the name of a file with the "true" parameters used in generating the field data.  
pvals.csv is the name of an output file that will hold the posterior distributions for the parameters and hyperparameters

The tool does the following:

1)	Reads in a list of parameters and associated uncertainties from test_inputs.csv (the header in that file specifies its format)
2)	Builds the base ISO model specified in examplemodel.ism
3)	Invokes the Morris method (in R) on the ISO model, varying the specified parameters between the �min� and �max� bounds given in test_inputs.csv
4)	Prints out the (normalized) sensitivity value for each parameter
5)	Selects a specified number of parameters with the highest sensitivity value
6)	Generates a Latin hypercube sample over a uniform distribution between the min and max of the selected parameters
7)	Runs the ISO model at these parameter values and records the energy use by month
8)	Generates an output file with monthly energy use from model runs, weather data and parameter values from LHS sample
9)	Generates similated field data based on the model, with randomly selected true parameters plus random variation
10)	Runs bayesian calibration (in R) to generated a sample from the posterior distribution for the selected parameters, given the simulated field data
11)	Generates figures showing posterior distributions
12)	Runs prediction code, saves results to files
13)	Generates figures showing predicted model results

You�ll need to make sure you have the �sensitivity�, "lhs", "ggplot2", "triangle", "gridExtra" and "car" packages in your R installation for this to work. 
If they are not insltalled, you can use "installRequiredPackages.rb" to install them.


