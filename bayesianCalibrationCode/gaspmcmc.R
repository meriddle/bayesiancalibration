
#===============================================================#
#     Kennedy and O'Hagan Model for Combining Field Data and    #
#       Computer Simulations for Calibration and Prediction     #    
#===============================================================#
  
# GASPMCMC Generate realizations from a posterior distribution
#
#          Use this function to:
#           1. Sequentially propose candidate parameter values
#           2. Use M-H algorithm to accept or reject
#           3. Repeat for specified number of iterations

# CALLS: logpost.m, gendist.m, gaspchcov.m
# CALLED BY: gaspdriver.m

#==============================================================%
#                        REQUIRED INPUTS                       %
#==============================================================%
# params0 structure containing information on:
#   theta: initial value for true calibration parameter value
#   beta_eta, lambda_eta: initial values for parameters for eta 
#   beta_b, lambda_b: initial values for parameters for bias
#   lambda_e: initial value of precision parameter for observation 
#             error
#   lambda_en: initial value of precision parameter for random error
#   theta_w: step width for generating candidate theta values
#   rho_eta_w: step width for generating candidate beta_eta values
#              rho_eta = exp(-beta_eta/4)
#   lambda_eta_w: step width for generating candidate lambda_eta values
#   rho_b_w: step width for generating candidate beta_b values
#            rho_b = exp(-beta_b/4)
#   lambda_b_w: step width for generating candidate lambda_b values
#   lambda_e_w: step width for generating candidate lambda_e values
#   lambda_en_w: step width for generating candidate lambda_en values
#   p: dimension of xf (xc)
#   q: dimension of theta (tc)
#   pq: p + q
#   n: number of field experiments
#   distz.d: distance array corresponding to z = [(xf,theta);(xc,tc)]
#   z: full set of inputs [(xf,theta);(xc,tc)]
#   sigmayCh: cholesky factorization of covariance matrix sigma_y
#   nparms: number of parameters
#   nmcmc: number of iterations of MCMC algorithm

#===============================================================%
#                           OUTPUTS                             %
#===============================================================%
# pvals: posterior realizations (nmcmc x numparms)
#        Each row is a realization from the posterior distribution
#        Each column corresponds to a parameter

# COMMENTS: Parameters are in the order of theta(q), beta_eta(p+q), 
#           beta_b(q), lambda_eta, lambda_en, lambda_b, lambda_e.
#           See model description below for more information on
#           how candidate values are generated and accepted/rejected.


#===============================================================%
#                          MODEL DETAILS                        %
#===============================================================%
  
# M-H algorithm for generating candidate values and accepting
# or rejecting:
  
# From current parameter value a0, generate candidate
# a1 ~ UNIF(a0 - .5w, a0 + .5w) where w is specified step width

# lpost0 = logpost(a0) ... value of logposterior distribution at a0
# lpost1 = logpost(a1) ... value of logposterior distribution at a1

# pi = lpost1 - lpost0
# if pi > 0: always accept a1
# if pi < 0: accept a1 with probability pi
#            stay at a0 with probability 1-pi

#===============================================================%
  
gaspmcmc = function(params){
  theta_w = params$theta_w;
  rho_eta_w = params$rho_eta_w;
  lambda_eta_w = params$lambda_eta_w;
  rho_b_w = params$rho_b_w;
  lambda_b_w = params$lambda_b_w;
  lambda_e_w = params$lambda_e_w;
  lambda_en_w = params$lambda_en_w;
  q = params$q;
  pq = params$pq;
  n = params$n;
  p = params$p;
  nparms = params$nparms;
  nmcmc = params$nmcmc;
  
  
    
  
  # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  # Storage for posterior realizations
  pvals <- matrix (0, nrow = nmcmc, ncol=nparms+2)    #indi=zeros(inds,1);
  
  
  # Start MCMC ...
  print(sprintf('First Calculation...'));
  # Evaluate logpost at params0
  adjparams0 = list();
  adjparams0$theta = params$theta;
  adjparams0$beta_eta = params$beta_eta;
  adjparams0$lambda_eta = params$lambda_eta;
  adjparams0$lambda_en = params$lambda_en;
  adjparams0$beta_b = params$beta_b;
  adjparams0$lambda_b = params$lambda_b;
  adjparams0$lambda_e = params$lambda_e;
  adjparams0$z <- params$z

  distz0 <- list()
  distz0$n <- nrow(params$z)
  #distz0$d <- rbind(params$distztl$d, adjparams0$distztr$d, params$distzbr$d)
  distztl <- gendist(params$z, params$ztlInds) # distance structure for top left part of z
  distztr <- gendist(params$z, params$ztrInds) # distance structure for top right part of z
  distzbr <- gendist(params$z, params$zbrInds) # distance structure for bottom left part of z
  
  distz0$d <- rbind(distztl$d, distztr$d, distzbr$d)

  sigma_eta_tri0 = gaspcovTri(distz0$d,adjparams0$beta_eta,adjparams0$lambda_eta);
  sigmayCh <- gaspchcov(distz0, adjparams0, params, sigma_eta_tri0)
  lpost0 = logpost(sigmayCh, adjparams0, params);
  adjparams1 = adjparams0;
  distz1 <- distz0
  sigma_eta_tri1 <- sigma_eta_tri0
  print(sprintf('Looping, n = %d ...', nmcmc));
  set.seed(1)

  for (iter in 1:nmcmc){
  
    # UPDATE theta
    for (k in 1:q){
      
      theta1_k = adjparams0$theta[k] + runif(1, -0.5, 0.5)*theta_w[k];
      #theta1_k <- 0.547208552958977 #(to match matlab)
      # CHECK valid support
      if ((theta1_k > -0.0) && (theta1_k < 1.0)){
        # Evaluate logpost at new value
        adjparams1$theta[k] = theta1_k;
        adjparams1$z[1:n,p+k] = theta1_k;
        
        
        dtemp = gendist(as.matrix(adjparams1$z[,p+k]), params$ztrInds);
        distz1$d[params$distztrInds, p+k] <- dtemp$d
        sigma_eta_tri1[params$distztrInds] = gaspcovTri(distz1$d[params$distztrInds,],adjparams1$beta_eta,adjparams1$lambda_eta);
        sigmayCh <- gaspchcov(distz1, adjparams1, params, sigma_eta_tri1)
        
        lpost1 = logpost(sigmayCh, adjparams1, params);
        # M-H acceptance step
        if (log(runif(1, 0, 1)) < (lpost1[1] - lpost0[1])){
          lpost0 = lpost1;
          adjparams0 = adjparams1;
          distz0 <- distz1
          sigma_eta_tri0 <- sigma_eta_tri1
        } else{
          adjparams1 = adjparams0;
          distz1 <- distz0
          sigma_eta_tri1 <- sigma_eta_tri0
        }
      }
    }
    
    # UPDATE beta_eta
    rho_eta = exp(-adjparams0$beta_eta/4);
    for (k in 1:pq){
      rho1_k = rho_eta[k] + runif(1, -0.5, 0.5)*rho_eta_w[k];
      # CHECK valid support
      if ((rho1_k > 0) && (rho1_k < 1)){
        # Evaluate logpost at new value
        beta1_k = -4*log(rho1_k);
        adjparams1$beta_eta[k] = beta1_k;

        sigma_eta_tri1 = gaspcovTri(distz0$d,adjparams1$beta_eta,adjparams1$lambda_eta);
        sigmayCh <- gaspchcov(distz0, adjparams1, params, sigma_eta_tri1)
        
        lpost1 = logpost(sigmayCh, adjparams1, params);
        # M-H acceptance step
        if (log(runif(1, 0, 1)) < (lpost1[1] - lpost0[1])){
          lpost0 = lpost1;
          adjparams0 = adjparams1;
          sigma_eta_tri0 <- sigma_eta_tri1
        } else{
          adjparams1 = adjparams0;
          sigma_eta_tri1 <- sigma_eta_tri0          
        }
      }
    }
    
    # UPDATE beta_b
    rho_b = exp(-adjparams0$beta_b/4);
    for (k in 1:p){
      rho1_k = rho_b[k] + runif(1, -0.5, 0.5)*rho_b_w[k];
      # CHECK valid support
      if ((rho1_k > 0) && (rho1_k < 1)){
        # Evaluate logpost at new value
        beta1_k = -4*log(rho1_k);
        adjparams1$beta_b[k] = beta1_k;
        
        sigmayCh <- gaspchcov(distz0, adjparams1, params, sigma_eta_tri1)
        
        lpost1 = logpost(sigmayCh, adjparams1, params);
        # M-H acceptance step
        if (log(runif(1, 0, 1)) < (lpost1[1] - lpost0[1])){
          lpost0 = lpost1;
          adjparams0 = adjparams1;
        } else{
          adjparams1 = adjparams0;
        }
      }
    }
    
    # UPDATE lambda_eta
    lambda_eta1 = adjparams0$lambda_eta + runif(1, -0.5, 0.5)*lambda_eta_w;
    # CHECK valid support
    if (lambda_eta1 > 0){
      # Evaluate logpost at new value
      adjparams1$lambda_eta = lambda_eta1;
      
      zr <- nrow(adjparams1$z)
      sigma_eta_tri1 = gaspcovTri(distz0$d,adjparams1$beta_eta,adjparams1$lambda_eta);
      sigmayCh <- gaspchcov(distz0, adjparams1, params, sigma_eta_tri1)
      
      lpost1 = logpost(sigmayCh, adjparams1, params);
      # M-H acceptance step
      if (log(runif(1, 0, 1)) < (lpost1[1] - lpost0[1])){
        lpost0 = lpost1;
        adjparams0 = adjparams1;
        sigma_eta_tri0 <- sigma_eta_tri1
      } else{
        adjparams1 = adjparams0;
        sigma_eta_tri1 <- sigma_eta_tri0
      }
    }
    
    # UPDATE lambda_en
    lambda_en1 = adjparams0$lambda_en + runif(1, -0.5, 0.5)*lambda_en_w;
    # CHECK valid support
    if (lambda_en1 > 0){      
      # Evaluate logpost at new value
      adjparams1$lambda_en = lambda_en1;
      
      sigmayCh <- gaspchcov(distz0, adjparams1, params, sigma_eta_tri1)
      
      lpost1 = logpost(sigmayCh, adjparams1, params);
      # M-H acceptance step
      if (log(runif(1, 0, 1)) < (lpost1[1] - lpost0[1])){
        lpost0 = lpost1;
        adjparams0 = adjparams1;
      } else{
        adjparams1 = adjparams0;
      }
    }
    
    # UPDATE lambda_b
    lambda_b1 = adjparams0$lambda_b + runif(1, -0.5, 0.5)*lambda_b_w;
    # CHECK valid support
    if (lambda_b1 > 0){
      # Evaluate logpost at new value
      adjparams1$lambda_b = lambda_b1;
      
      sigmayCh <- gaspchcov(distz0, adjparams1, params, sigma_eta_tri1)
      
      lpost1 = logpost(sigmayCh, adjparams1, params);
      # M-H acceptance step
      if (log(runif(1, 0, 1)) < (lpost1[1] - lpost0[1])){
        lpost0 = lpost1;
        adjparams0 = adjparams1;
      } else{
        adjparams1 = adjparams0;
      }
    }
    
    # UPDATE lambda_e
    lambda_e1 = adjparams0$lambda_e + runif(1, -0.5, 0.5)*lambda_e_w;
    # CHECK valid support
    if (lambda_e1 > 0){
      # Evaluate logpost at new value
      adjparams1$lambda_e = lambda_e1;
      
      sigmayCh <- gaspchcov(distz0, adjparams1, params, sigma_eta_tri1)
      
      lpost1 = logpost(sigmayCh, adjparams1, params);
      # M-H acceptance step
      if (log(runif(1, 0, 1)) < (lpost1[1] - lpost0[1])){
        lpost0 = lpost1;
        adjparams0 = adjparams1;
      } else{
        adjparams1 = adjparams0;
      }
    }
    
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~    
      pvals[iter,] = c(adjparams0$theta, t(adjparams0$beta_eta), t(adjparams0$beta_b), adjparams0$lambda_eta, adjparams0$lambda_en, adjparams0$lambda_b, adjparams0$lambda_e, lpost0[1], lpost0[2]);
    
    # Display iteration results on screen
    if (iter%%5 == 0){  
      print(sprintf('%4d: ',iter));
      #     fprintf(' %4.2f',pvals(iter,:));
      #        fprintf('\n');
    }
      
  }  
  return (pvals)
}