# call this after running runAll in ruby
# for this to work, the parameters and file names in lines 26 - 35 and the theta info 
# (lines 40-53) must be consistent with the ruby code that generates the files being read in

# also, if the number of calibration parameters changes, the scatterplotMatrix commands must
# be changed to generate those figures

source ("runmcmc.R")
source("gendist.R")
source("gaspmcmc.R")
source("logpost.R")
source("genUTInds.R")
source("genRectInds.R")
source("gaspcovTri.R")
source("gaspchcov.R")
source("gaspchcovOld.R")
source("gaspcov.R")
source("gaspcovFromTri.R")
source("density.R")
source("setupParams.R")
source("etaxpred.R")
source("yxpred.R")
source("rmvn.R")

#set parameters here
numWeatherVars = 2
nmcmc = 500
burnin = 100;
pvalsFilename = "pvals.csv"

#for prediction, use every n entries (where n = thinning)
#thinning = 1

#Rprof("Run2.rprof")
filename_com <- "cal_example_com.txt"
filename_field <- "cal_example_field.txt"

filename_expred <- "expred.csv"
filename_yyxpred <- "yyxpred.csv"
theta_info <- list()
theta_info[[1]] <- list()
theta_info[[1]]$name <- 'elecPowerAppliancesOccupied'
theta_info[[1]]$abbr = "\n\n\nelec\nPower\nAppl Occ";
theta_info[[1]]$prior <- list();
theta_info[[1]]$prior$type <- 'triangular';
theta_info[[1]]$prior$params <- c(5.32, 12.12, 8.11);

theta_info[[2]] <- list()
theta_info[[2]]$name <- 'coolingSystemCOP'
theta_info[[2]]$abbr = "\n\n\ncool sys\nCOP";
theta_info[[2]]$prior <- list();
theta_info[[2]]$prior$type <- 'triangular';
theta_info[[2]]$prior$params <- c(2, 4, 3.1);

num_theta = length (theta_info);

for(iter in 1:num_theta)
{
  theta_info[[iter]]$min <- theta_info[[iter]]$prior$params[1]
  theta_info[[iter]]$max <- theta_info[[iter]]$prior$params[2]
  #theta_info[[iter]]$start <- (theta_info[[iter]]$min + theta_info[[iter]]$max) / 2.0
}

params <- setupParams(theta_info, filename_com, filename_field, numWeatherVars, nmcmc, pvalsFilename)  



# pvals = read.csv(file.path("d:/anl/bayesian", "pvals.csv"));
pvals = read.csv(pvalsFilename);
#pvals = as.matrix(pvls))
pvals = tail(pvals, length(pvals[1])-burnin); # significant difference when showing burnin-data

#------------------------
library (ggplot2);
library (triangle);
library(gridExtra); # http://www.r-bloggers.com/extra-extra-get-your-gridextra/
library(car);
#------------------------
nBins = 32; # good: 100
dev.off(2);
#dev.new();
# windows();

plots  = list(); # List of plots, later to be up in grid
plots2 = list(); # List of plots, later to be up in grid
Vs  	 = list(); # List of V1,V2, ... Vn
# i = 4;
# for (i in 1:4)
for (i in 1:num_theta)
{
  #i_global <<- i; # make it global?
  #pvals_global <<- pvals; # make it global?
  #delta_x_global <<- delta_x; # make it global?
  
  windows();
  max_x = theta_info [[i]]$max;
  min_x = theta_info [[i]]$min;
  rang_x =  max_x - min_x;
  pvals[[i]] = pvals[[i]] * rang_x + min_x; # normalize pvals between min_x and max_x
  histo = hist(pvals[[i]], breaks=seq(min_x, max_x, l=nBins), plot=FALSE); # good: 50 bins
  
  # histo = histogram of pvals[i]
  # get total area of histogram
  pval_area = 0;
  delta_x = histo$mids[2] - histo$mids[1]; # width of histogram bars
  for (c in 1:length (histo$counts))
  {
    pval_area = pval_area + histo$counts[c] * delta_x;
  }
  
  # create triangular distribution
  
  x = seq (min_x, max_x, length.out=100); # good: 100 for straighter lines
  y = dtriangle (x, theta_info[[i]]$prior$params[1], theta_info[[i]]$prior$params[2],
                 theta_info[[i]]$prior$params[3]);
  
  # Area of triangle is 1, scale it's height so that
  # the area becames the area of the original histogram.
  
  ty = y * pval_area; # = altitude of ttiangle
  prior = data.frame (x, ty); # the prior (triangular) distribution
  
  Vs = c(Vs, pvals[[i]]); #---
  
  # plot the histogram and triangular dist
  plot1 = ggplot() +
    geom_histogram (data=pvals, aes(x=pvals[[i]]), binwidth=delta_x) +
    geom_line (data=prior, aes(x=x, y=ty),  color="red", size=1) +
    xlim (min(pvals[i], min_x),max(pvals[i], max_x)) +
    labs (x=theta_info[[i]]$name);
  if (FALSE) # Shows very strange behaviour!!!!
  {
    plots  = c(plots, list(plot1));
  }
  print (plot1);
  message ("--- plot ", i, " ---");
  message ("pval_area = ", pval_area);
  message ("nBins = ", nBins);
  message ("x range: ", min_x, " to ", max_x);
}
if (FALSE) # Shows very strange behaviour!!!!
{
  windows();
  do.call(grid.arrange, c(plots, ncol=4, main=paste("bandwidth =", delta_x)));
}


#------------------------------

if (num_theta == 2)  # include for Matt R. delivery
{
  windows();
  #if num_theta changes to N, need to change to ~V1+V2+V3+...+VN. Also change the if statement above
  scatterplotMatrix(~V1+V2, data=pvals, smoother=FALSE, diagonal="histogram", pch='.',
                    col="black", reg.line=FALSE, var.labels=NULL); # main="title"
  windows();
  #if num_theta changes to N, need to change to ~V1+V2+V3+...+VN.
  scatterplotMatrix(~V1+V2, data=pvals, smoother=FALSE, diagonal="density", pch='.',
                    reg.line=FALSE,
                    var.labels=c(theta_info[[1]]$abbr, theta_info[[2]]$abbr));
} else{
  message("scatter plot matrices not generated because scatterplotMatrix command is set up for 
          wrong number of parameters")
}


expred = as.matrix(read.csv("expred.csv"));
yyxpred = as.matrix(read.csv("yyxpred.csv"));

ym = params$ym
ysd = params$ysd
yf = params$yf
# Average over realizations
require("plyr")
# mmexpred = mean(expred);
# mmexpred = ((yyxpred[[1]])[1,] + (yyxpred[[1]])[2,]+ (yyxpred[[1]])[3,])  / 3; # mmyxpred = mean(yyxpred);
mmexpred = aaply(expred, 2, mean);
mmyxpred = aaply(yyxpred, 2, mean);
sdev     = aaply(yyxpred*ysd+ym, 2, sd); # 1 x 12
# varpred = var(yyxpred);  # THERE WAS A MISTAKE! MEAN WAS USED(OLD ONE: var(myyxpred)
varpred = aaply(yyxpred, 2, var);
sepred = sqrt(varpred);
means = mmyxpred * ysd + ym;

#################
require("ggplot2");
lv=month.abb[1:12];
Month=factor(lv, levels=lv);

#yf=t(yf);
#browser();
df = data.frame (Month, Means=means, history=yf[,1]);

plot12 = ggplot() +
  geom_errorbar(data=df, mapping=aes(x=Month, ymin=Means-2*sdev, ymax=Means+2*sdev),
                width=0.2, size=1, color="blue") +
  geom_point(data=df, mapping=aes(x=Month, y=Means), size=4, shape=21, fill="black") +
  geom_point(data=df, mapping=aes(x=Month, y=history), size=4, shape=21, fill="white") +
  ggtitle ("testPred 1");
windows ();
print (plot12);
#################

#
# figure(12)
# % plot(1:12,mmyxpred*ysd+ym, 'ro', 'MarkerSize',7);
# errorbar(1:12,mmyxpred*ysd+ym, 2*std(yyxpred*ysd+ym), 2*std(yyxpred*ysd+ym),'ro','MarkerSize',3,'MarkerFaceColor','r');
# hold on;
# xlabel('Month'); ylabel('Total Energy Consumption'); hold on;
# % Plot computer responses and field responses
# plot(1:12,yf,'ro','MarkerSize',5); hold on;
message ("Done with testPred.R");

#Rprof(NULL)
#summaryRprof("Run2.rprof")
