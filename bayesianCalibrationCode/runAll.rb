#This executes the parameter screening and experiment design steps
#Inputs:
#ARGV[0]: the ism model
#ARGV[1]: the parameter input file
#ARGV[2]: the filename used to store the model run results 
#ARGV[3]: the filename used to store the 'observed' data
#ARGV[4]: the filename used to store the 'true' parameters
#ARGV[5]: the filename used to store the results of the Bayesian calibration

$LOAD_PATH.unshift('C:\Program Files (x86)\OpenStudio 1.4.0\Ruby')

require 'openstudio'
require 'parameterInput'
require 'morrisRunner'
require 'lhsRunner'
require 'virtualBuilding'
require 'bCRunner'
require 'graphGenerator'
require 'predRunner'

com_filename = ARGV[2]
field_filename = ARGV[3]

#R.eval("install.packages('lhs')")
#R.eval("install.packages('sensitivity')")
#R.eval("install.packages('ggplot2')")
#R.eval("install.packages('triangle')")
#R.eval("install.packages('gridExtra')")
#R.eval("install.packages('car')")

# Initialize model and load parameters
userModel = OpenStudio::ISOModel::UserModel.new
userModel.load(OpenStudio::Path.new(ARGV[0]))
parameters = ParameterInput.read_input_file(ARGV[1])
energyModel = EnergyModel.new(userModel, parameters)

# Run morris method to calculate sensitivities for all parameters
numRunsPerParameter = 10
sensitivities = MorrisRunner.runMorris(energyModel, parameters, numRunsPerParameter)

# select parameters with highest sensitivities
numSelectedParameters = 2
selectedParametersWithMap = MorrisRunner.selectParameters(parameters, sensitivities, numSelectedParameters)
selectedParameters = selectedParametersWithMap[0]
parameterMap = selectedParametersWithMap[1]

# generate LHS sample
numModelRuns = 30
lhsSample = LhsRunner.generateLHSSample(selectedParameters, numModelRuns)
LhsRunner.writeToFile(lhsSample, "checkLHS.txt")

# we have to be sure to set not-selected parameters to desired values: I've used
# the "mid" values from the parameter input file
midValues = ParameterInput.getMidValues(parameters)

# run model at LHS sample values, and pull out monthly total energy use from results
monthlyEnergyData = []
fullParameterSamples = []
for i in 0..numModelRuns-1
	#combine the LHS sample for selected params with "mid" values for non-selected params
	fullParameterSample = LhsRunner.combineParamVals(midValues, lhsSample[i], parameterMap)
	fullParameterSamples << fullParameterSample
	monthlyEnergyData << energyModel.runMonthly(fullParameterSample)
	#modelResults = energyModel.run(lhsSample[i])
	#monthlyEnergyData << energyModel.getMonthlyEnergy(modelResults)
end
LhsRunner.writeToFile(fullParameterSamples, "checkFullParameterSamples.txt")
weatherData = energyModel.getWeatherVars()

# save results of model run, along with parameter values used and weather data,
# to computer data file
LhsRunner.generateComFile(monthlyEnergyData, weatherData, lhsSample, ARGV[2])

# generate "field" data to calibrate model against
# first, select true parameter values (drawn randomly from uniform distribution 
# between min and max)
trueParameterVals = VirtualBuilding.generateRandomVals(selectedParameters)
VirtualBuilding.writeArrayToFile(trueParameterVals, ARGV[4])
# combine true values with mid values for non-selected parameters, and run model
fullTrueParameterVals = LhsRunner.combineParamVals(midValues, trueParameterVals, parameterMap)
monthlyEnergyData = energyModel.runMonthly(fullTrueParameterVals)
# adjust model results by random amount, (currently from independent uniform 
# distributions for each month)
maxPercentChange = 0.1
observedData = VirtualBuilding.addRandomVariation(monthlyEnergyData, maxPercentChange)
# save results and weather data to field data file
VirtualBuilding.generateFieldFile(observedData, weatherData, ARGV[3])

# run Bayesian calibration, given field data and computer data, for selected parameters
# a sample from the posterior distribution is saved to pvals.csv (except some at beginning should be dropped as burn-in) 
numMCMCSteps = 500
BCRunner.runBC(selectedParameters, ARGV[2], ARGV[3], weatherData[0].length, numMCMCSteps, ARGV[5])

#burnin = 5
burnin = 100
GraphGenerator.graphPosteriors(selectedParameters, ARGV[5], burnin)
thinning = 5

expred_filename = "expred.csv"
yyxpred_filename = "yyxpred.csv"
PredRunner.runPred(selectedParameters, ARGV[2], ARGV[3], weatherData[0].length, numMCMCSteps, burnin, thinning, ARGV[5], expred_filename, yyxpred_filename)
GraphGenerator.graphPred(com_filename, field_filename, expred_filename, yyxpred_filename)
