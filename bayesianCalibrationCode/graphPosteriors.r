#to graph scatterplot matrices with different numbers of parameters, the code in lines 111-133 needs to be changed 
#(see comments at those lines for details)

graphPosteriors <- function(theta_info, pvals_filename, burnin){

	num_theta = length (theta_info);
	for(iter in 1:num_theta)
	{
	  theta_info[[iter]]$min <- theta_info[[iter]]$prior$params[1]
	  theta_info[[iter]]$max <- theta_info[[iter]]$prior$params[2]
	  #theta_info[[iter]]$start <- (theta_info[[iter]]$min + theta_info[[iter]]$max) / 2.0
	}
	# pvals = read.csv(file.path("d:/anl/bayesian", "pvals.csv"));
	 pvals = read.csv(pvals_filename);
	#pvals = as.matrix(pvls))
	#nmcmc = 30000; # ???? why is this needed? mjj
	#burnin = 1000;
	pvals = tail(pvals, length(pvals[1])-burnin); # significant difference when showing burnin-data

	#------------------------
	library (ggplot2);
	library (triangle);
	library(gridExtra); # http://www.r-bloggers.com/extra-extra-get-your-gridextra/
	library(car);
	#------------------------
	nBins = 32; # good: 100
	dev.off(2);
	#dev.new();
	# windows();

	plots  = list(); # List of plots, later to be up in grid
	plots2 = list(); # List of plots, later to be up in grid
	Vs		 = list(); # List of V1,V2, ... Vn
	# i = 4;
	# for (i in 1:4)
	for (i in 1:num_theta)
	{
	 #message("in loop")
	 i<<-i; # make it global?
	 max_x = theta_info [[i]]$max;
	 min_x = theta_info [[i]]$min;
	 rang_x =  max_x - min_x;
	 pvals[[i]] = pvals[[i]] * rang_x + min_x; # normalize pvals between min_x and max_x
	 #message("adjusted pvals")

	 histo = hist(pvals[[i]], breaks=seq(min_x, max_x, l=nBins), plot=FALSE); # good: 50 bins
	 #message("created histogram")

	 pvals<<-pvals; # make it global?

										   # histo = histogram of pvals[i]
										   # get total area of histogram
	 pval_area = 0;
	 delta_x = histo$mids[2] - histo$mids[1]; # width of histogram bars
	 delta_x <<- delta_x; # make it global?
	 for (c in 1:length (histo$counts))
		{
		 pval_area = pval_area + histo$counts[c] * delta_x;
		}

										   # create triangular distribution

	x = seq (min_x, max_x, length.out=100); # good: 100 for straighter lines
	y = dtriangle (x, theta_info[[i]]$prior$params[1], theta_info[[i]]$prior$params[2],
					  theta_info[[i]]$prior$params[3]);
	 #message("generated trangle ys")

														# Area of triangle is 1, scale it's height so that
														# the area becames the area of the original histogram.

	ty = y * pval_area; # = altitude of ttiangle
	prior = data.frame (x, ty); # the prior (triangular) distribution
	#message("made triangle data frame")

	Vs = c(Vs, pvals[[i]]); #---

									   # plot the histogram and triangular dist
	#plot1=mjj (pvals, i, delta_x)
	 plot1 <- ggplot() +
		  geom_histogram (data=pvals, aes(x=pvals[[i]]), binwidth=delta_x) +
		   geom_line (data=prior, aes(x=x, y=ty),  color="red", size=1) +
			xlim (min(pvals[i], min_x),max(pvals[i], max_x)) +
		  labs (x=theta_info[[i]]$name);
	
	
	if (FALSE) # Shows very strange behaviour!!!!
	{
	 plots  = c(plots, list(plot1));
	}
	 windows();
	 figureName <- paste("PosteriorVsPrior", as.character(i), ".jpg")
     jpeg(figureName)
	 print (plot1);
	 dev.off()
	 #message ("--- generated plot ", i, " ---");
	 #message ("pval_area = ", pval_area);
	 #message ("nBins = ", nBins);
	 #message ("x range: ", min_x, " to ", max_x);
	}


	if (FALSE) # Shows very strange behaviour!!!!
		{
		windows();
		do.call(grid.arrange, c(plots, ncol=4, main=paste("bandwidth =", delta_x)));
		}


	#------------------------------

	if (num_theta == 2)  # include for Matt R. delivery
	{
	  windows();
      jpeg("posteriorScatterPlotsV1.jpg")
	  #if num_theta changes to N, need to change to ~V1+V2+V3+...+VN. Also change the if statement above to if (num_theta == N)
	  scatterplotMatrix(~V1+V2, data=pvals, smoother=FALSE, diagonal="histogram", pch='.',
						col="black", reg.line=FALSE, var.labels=NULL); # main="title"
	  dev.off()
	  windows();
      jpeg("posteriorScatterPlotsV2.jpg")
	  var_labels = rep(0, times = num_theta)
	  for (i2 in 1:num_theta){
	    var_labels[i2] = theta_info[[i2]]$name
	  }
	  #if num_theta changes to N, need to change to ~V1+V2+V3+...+VN.
	  scatterplotMatrix(~V1+V2, data=pvals, smoother=FALSE, diagonal="density", pch='.',
						reg.line=FALSE,
						var.labels=var_labels);
	  dev.off()
	} else{
	  message("scatter plot matrices not generated because scatterplotMatrix command is set up for 
			  wrong number of parameters")
	}
}